angular.module('myApp', []).controller('productCtrl', function ($scope) {
    $scope.products = [
        {
        	name:'Calendar',
        	image: "img/portfolio/cabin.png",
        	price: "29.95",
        	description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec posuere elementum malesuada."
        },
        {
        	name:'Communicator',
        	image: "img/portfolio/cake.png",
        	price: "49.99",
        	description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec posuere elementum malesuada."
        },
    	{
        	name:'Inventory',
        	image: "img/portfolio/circus.png",
        	price: "99.99",
        	description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec posuere elementum malesuada."
        },

    ];
});