﻿angular.module('app').controller('mainCtrl', function ($scope, $rootScope) {
    $rootScope.__loggedIn = false;  //This is very bad.  Don't use rootScope like this EVER!!!  This is just a very fast way to get a global variable in.  Normally you'd use a service for these things
    $rootScope.__itemsInCart = 0;
    $rootScope.__isLoggingIn = false;
    $scope.startLogin = function () {
        $rootScope.__isLoggingIn = true;
    }
    $scope.stopLogin = function () {
        $rootScope.__isLoggingIn = false;
    }
    $scope.startLogout = function () {
        $rootScope.__loggedIn = false;
    }

    $scope.personGroup = [
        {
            profilePic: "./images/user-icon.png",
            name: "Bob",
            profession: "Artist",
            productsUsed: [
                {
                    item: "portfolio",
                    description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec posuere elementum malesuada."
                },
                {
                    item: "blog",
                    description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec posuere elementum malesuada."
                },
                {
                    item: "communicator",
                    description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec posuere elementum malesuada."
                }
            ]
        },
        {
            profilePic: "./images/user-icon.png",
            name: "Susan",
            profession: "Hairstylist",
            productsUsed: [
                {
                    item: "portfolio",
                    description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec posuere elementum malesuada."
                },
                {
                    item: "blog",
                    description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec posuere elementum malesuada."
                },
                {
                    item: "calendar",
                    description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec posuere elementum malesuada."
                },
                {
                    item: "communicator",
                    description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec posuere elementum malesuada."
                },
                {
                    item: "invoicer",
                    description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec posuere elementum malesuada."
                }
            ]
        },
        {
            profilePic: "./images/user-icon.png",
            name: "Abby",
            profession: "Retail",
            productsUsed: [
                {
                    item: "inventory",
                    description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec posuere elementum malesuada."
                },
                {
                    item: "logistics",
                    description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec posuere elementum malesuada."
                },
                {
                    item: "project manager",
                    description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec posuere elementum malesuada."
                }
            ]
        },
        {
            profilePic: "./images/user-icon.png",
            name: "Jerry",
            profession: "Baker",
            productsUsed: [
                {
                    item: "inventory",
                    description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec posuere elementum malesuada."
                },
                {
                    item: "portfolio",
                    description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec posuere elementum malesuada."
                }
            ]
        }
    ]

    $scope.products = [
        {
            productId: 1,
            name:'Calendar',
            image: "img/portfolio/calendar.png",
            price: 29.92,
            description: "A calendar can help you see your day to day schedule at a glance. It is also very helpful for planning your work year as well as setting up meetings with clients, and vendors"
        },
        {
            productId: 2,
            name:'Communicator',
            price: 60,
            image: "img/portfolio/communicator.png",
            description: "In todays busy world, communication is key. Whether you need to keep in touch with your suppliers, or solve issues with clients this product is a must!"
        },
        {
            productId: 3,
            name: 'Inventory',
            price: 159.99,
            image: "img/portfolio/inventory.png",
            description: "Tired of keeping track of all your stuff with paper and pencil?  Are you wondering if there's a better way to do all the things?  Has all hope fleeted from your eyes?  If you said 'Yes' to any of these things, try our new Inventory Management System by SCAMco!"
        },

    ];
});