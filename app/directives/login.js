﻿angular.module("app").directive("login", function () {
    return {
        templateUrl: "app/directives/login.html",
        restrict: "E",
        scope: {
            
        },
        controller: function ($scope, $rootScope) {
            $scope.login = function () {
                $rootScope.__loggedIn = true;
                $rootScope.__isLoggingIn = false;
            }
        }
    }
})();