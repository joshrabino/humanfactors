﻿angular.module("app").directive("products", function() {
    return {
        templateUrl: "app/directives/products.html",
        restrict: "E",
        scope: {
            products: "="
        },
        controller: function ($scope, $rootScope) {
            $scope.productsTab = -1;
            for (i = 0; i < $scope.products.length; i++) {
                $scope.products[i].inCart = false;
            }
        
            $scope.isProductSelected = function (checkTab) {
                return $scope.productsTab === checkTab;
            };

            $scope.selectProduct = function (tab) {
                if ($scope.productsTab == tab) {
                    $scope.productsTab = -1;
                } else {
                    $scope.productsTab = tab;
                }
            };
            
            $scope.isLoggedIn = function () {
                return $rootScope.__loggedIn;
            }
        
            $scope.addToCart = function (product) {
                $rootScope.__itemsInCart = $rootScope.__itemsInCart + 1;
                product.inCart = true;
            }
            $scope.removeFromCart = function (product) {
                $rootScope.__itemsInCart = $rootScope.__itemsInCart - 1;
                product.inCart = false;
            }
        }
    }
})();