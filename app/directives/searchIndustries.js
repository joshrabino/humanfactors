﻿angular.module("app").directive("searchIndustries", function() {
    return {
        templateUrl: "app/directives/searchIndustries.html",
        restrict: "E",
        scope: {
            people: "="
        },
        controller: function ($scope, $rootScope) {
            for (i = 0; i < $scope.people.length; i++) {
                $scope.people[i].canSee = false;
                $scope.people[i].isExpanded = false;
                for (j = 0; j < $scope.people[i].productsUsed.length; j++)
                {
                    $scope.people[i].productsUsed[j].inCart = false;
                }
            }

            $scope.filterList = function (filterString) {
                for (i = 0; i < $scope.people.length; i++) {
                    person = $scope.people[i];
                    if (filterString == undefined | filterString == "")
                    {
                        person.canSee = true;
                    }
                    else
                    {
                        person.canSee = applyFilter(person, filterString);
                    }
                }
            }

            applyFilter = function (person, filterString) {
                filterString = filterString.toUpperCase();
                if (person.name.toUpperCase().indexOf(filterString) > -1
                    | person.profession.toUpperCase().indexOf(filterString) > -1)
                {
                    return true;
                }
                return false;
            }

            $scope.expand = function (person)
            {
                if (person.isExpanded) {
                    person.isExpanded = false;
                }
                else {
                    person.isExpanded = true;
                }
                setTimeout(function () {
                    var id = person.name + person.profession
                    var ele = document.getElementById(id);
                    window.scrollTo(ele.offsetLeft, ele.offsetTop-110);
                }, 25);
            }

            $scope.addToCart = function (product) {
                $rootScope.__itemsInCart = $rootScope.__itemsInCart + 1;
                product.inCart = true;
                return false;
            }
            $scope.removeFromCart = function (product) {
                $rootScope.__itemsInCart = $rootScope.__itemsInCart - 1;
                product.inCart = false;
                return false;
            }
        }
    }
})();